/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author pranalinarkar
 */
public class VitalSignHistory {
    private ArrayList<VitalSign> vitalSignList;
    
    public VitalSignHistory()
    {this.vitalSignList=new ArrayList<>();}

    public ArrayList<VitalSign> getVitalSignList() {
        return vitalSignList;
    }
     public void setVitalSignList(ArrayList<VitalSign> vitalSignList) {
        this.vitalSignList = vitalSignList;
    }
    public void deleteVitalSign(VitalSign vitalsign)
    {
    vitalSignList.remove(vitalsign);}
   
   public VitalSign addVitalSign(){
   VitalSign v=new VitalSign();
   vitalSignList.add(v);
           return v;
   }
}
