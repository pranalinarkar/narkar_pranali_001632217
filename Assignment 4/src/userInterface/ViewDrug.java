/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface;

import business.Drug;
import business.DrugDirectory;
import business.InventoryItems;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author pranalinarkar
 */
public class ViewDrug extends javax.swing.JPanel {
private JPanel upc;
private DrugDirectory drugDirectory;
    /**
     * Creates new form ViewDrug
     */
    public ViewDrug(JPanel upc,DrugDirectory drugDirectory) {
        initComponents();
         this.upc=upc;
         this.drugDirectory=drugDirectory;
         display();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSave = new javax.swing.JButton();
        jUpdate = new javax.swing.JButton();
        jBack = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jDrug = new javax.swing.JTable();
        jview = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jManufacturer = new javax.swing.JTextField();
        jPrice = new javax.swing.JTextField();
        jExpdate = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jManufactringdate = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jBatchNumb = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jcategory = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jDrugID = new javax.swing.JTextField();
        jDelete = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jSave.setText("save");
        jSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jSaveActionPerformed(evt);
            }
        });
        add(jSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(225, 366, -1, -1));

        jUpdate.setText("update");
        jUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jUpdateActionPerformed(evt);
            }
        });
        add(jUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(429, 366, -1, -1));

        jBack.setText("<<back");
        jBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBackActionPerformed(evt);
            }
        });
        add(jBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 366, -1, -1));

        jDrug.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Drug Name"
            }
        ));
        jScrollPane1.setViewportView(jDrug);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 11, -1, 93));

        jview.setText("View");
        jview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jviewActionPerformed(evt);
            }
        });
        add(jview, new org.netbeans.lib.awtextra.AbsoluteConstraints(322, 366, -1, -1));

        jLabel8.setText("Batch Number");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 271, -1, -1));

        jLabel9.setText("Category");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 299, -1, -1));
        add(jManufacturer, new org.netbeans.lib.awtextra.AbsoluteConstraints(346, 119, 76, -1));
        add(jPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(346, 157, 76, -1));
        add(jExpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(346, 195, 76, -1));

        jLabel4.setText("Manufacturer");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 122, -1, -1));
        add(jManufactringdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(346, 233, 76, -1));

        jLabel5.setText("Price");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 160, -1, -1));

        jBatchNumb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBatchNumbActionPerformed(evt);
            }
        });
        add(jBatchNumb, new org.netbeans.lib.awtextra.AbsoluteConstraints(346, 265, 76, -1));

        jLabel6.setText("ExpDate");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 198, -1, -1));
        add(jcategory, new org.netbeans.lib.awtextra.AbsoluteConstraints(346, 296, 76, -1));

        jLabel7.setText("Manufacturing date");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(106, 236, -1, -1));

        jLabel1.setText("Drug ID");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 331, -1, -1));

        jDrugID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jDrugIDActionPerformed(evt);
            }
        });
        add(jDrugID, new org.netbeans.lib.awtextra.AbsoluteConstraints(347, 328, 75, -1));

        jDelete.setText("delete");
        jDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jDeleteActionPerformed(evt);
            }
        });
        add(jDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(123, 366, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void jBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBackActionPerformed
       upc.remove(this);
        CardLayout layout= (CardLayout)upc.getLayout();
        layout.previous(upc);
    }//GEN-LAST:event_jBackActionPerformed

    private void jUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jUpdateActionPerformed
        jBatchNumb.setEnabled(true);
        
        jManufactringdate.setEnabled(true);
        jExpdate.setEnabled(true);
        jPrice.setEnabled(true);
        jcategory.setEnabled(true);
        jManufacturer.setEnabled(true);
        jSave.setEnabled(true);
        jUpdate.setEnabled(false);
    }//GEN-LAST:event_jUpdateActionPerformed

    private void jSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jSaveActionPerformed
        
        int batchnumber = Integer.parseInt(jBatchNumb.getText());
         int price = Integer.parseInt(jPrice.getText());
        String manufacturer = jManufacturer.getText();
        String manufacturingdate= jManufactringdate.getText();
        String expdate= jExpdate.getText();
        String category= jcategory.getText();
        Drug drug=drugDirectory.getDrug();
        drug.setBatchNumb(batchnumber);
        drug.setCatagory(category);
        drug.setManufacturer(manufacturer);
        
        drug.setExpDate(expdate);
        drug.setManuDate(manufacturingdate);
        drug.setPrice(price);
        JOptionPane.showMessageDialog(null, "Drug is saved");
        jSave.setEnabled(false);
        jUpdate.setEnabled(true);
    }//GEN-LAST:event_jSaveActionPerformed

    private void jviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jviewActionPerformed
        int selectedRow=jDrug.getSelectedRow();
          
if(selectedRow>=0)
{
    
    Drug d= (Drug)jDrug.getValueAt(selectedRow, 0);
    jDrugID.setText(String.valueOf(d.getDrugID()));
    jBatchNumb.setText(String.valueOf(d.getBatchNumb()));
    jPrice.setText(String.valueOf(d.getPrice()));
    jExpdate.setText(d.getExpDate());
    jManufactringdate.setText(d.getManuDate());
    jManufacturer.setText(d.getManufacturer());
    jcategory.setText(d.getCatagory());
    //jD.setText(String.valueOf(vs.getTimeStamp()));
    
}// TODO {}add your handling code here:
else 
JOptionPane.showConfirmDialog(null, "please select a row first", "Warning", JOptionPane.WARNING_MESSAGE);
    }//GEN-LAST:event_jviewActionPerformed

    private void jBatchNumbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBatchNumbActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jBatchNumbActionPerformed

    private void jDrugIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jDrugIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jDrugIDActionPerformed

    private void jDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jDeleteActionPerformed
        int row = jDrug.getSelectedRow();
        
        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Drug d = (Drug) jDrug.getValueAt(row, 0);
        drugDirectory.removeDrug(d);
        jManufacturer.setText("");
         jPrice.setText("");
         jBatchNumb.setText("");
         jManufactringdate.setText("");
         jExpdate.setText("");
         jcategory.setText("");
          jDrugID.setText("");
         
        refreshTable();
    }//GEN-LAST:event_jDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBack;
    private javax.swing.JTextField jBatchNumb;
    private javax.swing.JButton jDelete;
    private javax.swing.JTable jDrug;
    private javax.swing.JTextField jDrugID;
    private javax.swing.JTextField jExpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField jManufactringdate;
    private javax.swing.JTextField jManufacturer;
    private javax.swing.JTextField jPrice;
    private javax.swing.JButton jSave;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jUpdate;
    private javax.swing.JTextField jcategory;
    private javax.swing.JButton jview;
    // End of variables declaration//GEN-END:variables

    private void display() {
         DefaultTableModel dtm=(DefaultTableModel)jDrug.getModel();
        int rowcount = jDrug.getRowCount();
        for(int i=rowcount-1;i>=0;i--){
        dtm.removeRow(i);}
//To change body of generated methods, choose Tools | Templates.
        //ArrayList<VitalSign> arrayList=patient.getVitalSignHistory().getVitalSignList();
        for(Drug d: drugDirectory.getDrugDirectory())
        {
           Object row[] = new Object[1];
           row[0]=d;
          
           
           dtm.addRow(row);
        }
    }

    private void refreshTable() {
         int rowCount = jDrug.getRowCount();
        DefaultTableModel model = (DefaultTableModel)jDrug.getModel();
        for(int i=rowCount-1;i>=0;i--) {
            model.removeRow(i);
        }
        
        for(Drug d : drugDirectory.getDrugDirectory()) {
            Object row[] = new Object[1];
            row[0] = d;
            
            model.addRow(row);
        }
    }
}
