/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author pranalinarkar
 */
public class DrugInventory {
    private InventoryItems inventoryItems;
    private ArrayList<InventoryItems> inventoryList;
    
   public DrugInventory(){
       this.inventoryList= new ArrayList<>();
        
    }

    public InventoryItems getInventoryItems() {
        return inventoryItems;
    }

    public void setInventoryItems(InventoryItems inventoryItems) {
        this.inventoryItems = inventoryItems;
    }

    public ArrayList<InventoryItems> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(ArrayList<InventoryItems> inventoryList) {
        this.inventoryList = inventoryList;
    }
   public InventoryItems addInventoryItem(Drug d,int q, int price){
    InventoryItems inventoryItems= new InventoryItems();
       inventoryItems.setDrug(d);
       inventoryItems.setQuantity(q);
       inventoryItems.setSalesPrice(price);
  
       inventoryList.add(inventoryItems);
       return inventoryItems;
   }
   public void deleteInventoryItems(InventoryItems inventoryItems){
       inventoryList.remove(inventoryItems);
   }
    
}
