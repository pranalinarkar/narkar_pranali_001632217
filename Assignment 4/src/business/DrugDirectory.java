/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pranalinarkar
 */
public class DrugDirectory {
     private List<Drug> drugDirectory;
    private Drug drug;
    public DrugDirectory() {
        drugDirectory = new ArrayList<Drug>();
    }

    public List<Drug> getDrugDirectory() {
        return drugDirectory;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    

    public void setDrugDirectory(List<Drug> drugDirectory) {
        this.drugDirectory = drugDirectory;
    }

   

    
    
    public Drug addDrug() {
         Drug d = new Drug();
        drugDirectory.add(d);
        return d;
    }
    
    public void removeDrug(Drug d) {
        drugDirectory.remove(d);
    }
    
    public Drug searchDrug(int id) {
        //ArrayList<Product> result = new ArrayList<Product>();
        for(Drug d : drugDirectory) {
            if(d.getDrugID()== id) {
                return d;
            }
        }
        return null;
    }
}
