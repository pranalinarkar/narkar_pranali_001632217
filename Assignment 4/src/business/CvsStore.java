/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author pranalinarkar
 */
public class CvsStore {
    private DrugInventory drugInventory;
    private String storeName;
    private String location;
    
    public CvsStore(){
           drugInventory=new DrugInventory();
    }

    public DrugInventory getDrugInventory() {
        return drugInventory;
    }

    public void setDrugInventory(DrugInventory drugInventory) {
        this.drugInventory = drugInventory;
    }

   

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return storeName;
    }
    
    
}
