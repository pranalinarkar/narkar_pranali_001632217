/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pranalinarkar
 */
public class CvsStoreDirectory {
    private List<CvsStore> cvsStoreDirectory;
    public CvsStoreDirectory() {
    
        cvsStoreDirectory = new ArrayList<CvsStore>();
    }

    public List<CvsStore> getCvsStoreDirectory() {
        return cvsStoreDirectory;
    }

    public void setCvsStoreDirectory(List<CvsStore> cvsStoreDirectory) {
        this.cvsStoreDirectory = cvsStoreDirectory;
    }
    
    
    
    
    public CvsStore addStore(){
        CvsStore c = new CvsStore();
        cvsStoreDirectory.add(c);
        return c;
    }
    
    public void removeStore(CvsStore c){
        cvsStoreDirectory.remove(c);
    }
    
    public CvsStore searchStore(String keyword){
        for (CvsStore c : cvsStoreDirectory) {
            if(c.getStoreName().equals(keyword)){
                return c;
            }
        }
        return null;
    }
}
