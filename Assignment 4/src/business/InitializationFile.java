/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author pranalinarkar
 */
public class InitializationFile {
    
    public void setInitial(DrugDirectory drugDirectory)
    {
        DrugDirectory d = drugDirectory;
         
        Drug d1 = d.addDrug();
        
        d1.setDrugName("crocin");
        d1.setManufacturer("abc");
        d1.setPrice(40);
        d1.setBatchNumb(50);
        d1.setCatagory("normal");
        d1.setExpDate("05/24/2018");
        d1.setManuDate("05/24/2015");
        
        
        Drug d2=d.addDrug();
        d2.setDrugName("combiflame");
        d2.setManufacturer("xyz");
        d2.setPrice(50);
        d2.setBatchNumb(60);
        d2.setCatagory("normal");
        d2.setExpDate("05/24/2018");
        d2.setManuDate("05/24/2015");
        
        
    }
    
    
    
}
