/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;


/**
 *
 * @author pranalinarkar
 */
public class InventoryItems {
    private Drug drug;
    private int quantity;
    private int salesPrice;
    private int drugSNumber;
    private static int count=0;
    
    public int getSalesPrice() {
        return salesPrice;
    }
   
    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public InventoryItems(){
count++;
    drugSNumber=count;

}
    

    public int getDrugSNumber() {
        return drugSNumber;
    }

    public void setDrugSNumber(int drugSNumber) {
        this.drugSNumber = drugSNumber;
    }
   
    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

   
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return drug.getDrugName();
    }

   
    
}
