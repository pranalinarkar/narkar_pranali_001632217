/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;
import java.util.ArrayList;
/**
 *
 * @author pranalinarkar
 */
public class PersonList {
    private ArrayList<Person> personList;

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
    
    public PersonList(){
this.personList=new ArrayList<>();


}
    
public Person addperson()
        {
       Person p= new Person();
        personList.add(p);
      
    return(p);
}

public void deleteperson(Person p)
    {
        personList.remove(p);
    }

public Person searchPerson(String name)
    {
       for(Person p: personList) 
       {
           if(name.equals(p.getName()))
               return p;
       }
       return null;
        
    }


}
