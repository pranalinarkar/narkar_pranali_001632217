/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author pranalinarkar
 */
public class VitalSign {
 private int respiratoryRate;
 private int heartRate;
 private int systolicBloodPressure;
 private int weightInPound;
 //private Date timeStamp;
 private String status;
 private VitalSign vs;
 private Patient p;
 private String timestamp;
 private Date date;

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    

    public VitalSign getVs() {
        return vs;
    }

    public void setVs(VitalSign vs) {
        this.vs = vs;
    }

    public Patient getP() {
        return p;
    }

    public void setP(Patient p) {
        this.p = p;
    }
 
 
 public String getStatus(Patient p, VitalSign vs)
 {//this.vs=vs;
 //this.p=p;
 int age=p.getAge();
 int res=vs.getRespiratoryRate();
 int hr=vs.getHeartRate();
 int sbp=vs.getSystolicBloodPressure();
 int w=vs.getWeightInPound();
 if(age<=3&&age>=1){
 if(res>=20&&res<=30&&hr>=80&&hr<=130&&sbp>=80&&sbp<=110&&w<=31&&w>=22)
     status="Normal";
     else 
     status="Abnormal";
 }
 else if (age<=5&&age>=4){
 if(res>=20&&res<=30&&hr>=80&&hr<=120&&sbp>=80&&sbp<=110&&w<=40&&w>=31)
     status="Normal";
 else
     status="Abnormal";}
 else if (age<=12&&age>=6){
 if(res>=20&&res<=30&&hr>=70&&hr<=110&&sbp>=70&&sbp<=120&&w<=92&&w>=41)
     status="Normal";
 else
     status="Abnormal";}
 else if (age>13){
 if(res>=12&&res<=20&&hr>=55&&hr<=105&&sbp>=110&&sbp<=120&&w>110)
     status="Normal";
 else
     status="Abnormal";}
 else status="Abnormal";
     return status;
 }
      
 

    public int getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(int respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(int systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    public int getWeightInPound() {
        return weightInPound;
    }

    public String getTimestamp() {
        String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(date);
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return this.timestamp;
    }

    public void setWeightInPound(int weightInPound) {
        this.weightInPound = weightInPound;
    }

   

 
 
    
}
