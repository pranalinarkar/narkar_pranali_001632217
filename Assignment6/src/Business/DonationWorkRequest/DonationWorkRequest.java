/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.DonationWorkRequest;

import Business.Donation.Donation;
import Business.WorkQueue.LabTestWorkRequest;

/**
 *
 * @author pranalinarkar
 */
public class DonationWorkRequest extends LabTestWorkRequest{
    
    private Donation donation;
    public DonationWorkRequest(){
    
    donation=new Donation();
    
    }

    public Donation getDonation() {
        return donation;
    }

    public void setDonation(Donation donation) {
        this.donation = donation;
    }
    
    
}
