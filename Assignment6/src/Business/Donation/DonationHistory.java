/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donation;

import Business.DonationWorkRequest.DonationWorkRequest;
import Business.DonorOrg.DonorOrg;
import Business.Employee.Employee;
import Business.Employee.EmployeeDirectory;
import Business.Enterprise.Enterprise;
import Business.Organization.DonorOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkRequest;
import java.util.ArrayList;

/**
 *
 * @author pranalinarkar
 */
public class DonationHistory {
 private ArrayList<Donation> donationList;
 private DonationWorkRequest donationWorkRequest;
 private UserAccount userAccount;
 private WorkRequest req;
 private EmployeeDirectory employeeDirectory;
 private Enterprise enterprise;
 public DonationHistory(){
 
 donationList=new ArrayList<>();
 }

    public ArrayList<Donation> getDonationList() {
        return donationList;
    }

    public void setDonationList(ArrayList<Donation> donationList) {
        this.donationList = donationList;
    }
 
  public WorkRequest searchDonor(String dname){
    WorkRequest req=null;
       for(WorkRequest req1: userAccount.getWorkQueue().getWorkRequestList()) {
         if(req1 instanceof DonationWorkRequest)
         {
           req=  req1;
           break;
             
             
         }
       
       
       }
       if(req!=null){
       Organization or=null;
       for(Organization or1:enterprise.getOrganizationDirectory().getOrganizationList() ){
           
           if(or1 instanceof DonorOrganization){
               or=or1;
               break;
           }
           
           if(or!=null){
           for (Employee emp : or.getEmployeeDirectory().getEmployeeList()) {
if(emp.getName().equals(dname)){
    return req;
}          
       }
           }
           }
         
       
       }
            
        
       

     return null;
  
    }
}
