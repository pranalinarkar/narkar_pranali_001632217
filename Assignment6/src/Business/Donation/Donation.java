/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donation;

import java.util.Date;

/**
 *
 * @author pranalinarkar
 */
public class Donation {
    private String bloodType;
    private String barCode;
    private Date bloodDonateDate;
    
    private DonationHistory donationHistory;

    public DonationHistory getDonationHistory() {
        return donationHistory;
    }

    public void setDonationHistory(DonationHistory donationHistory) {
        this.donationHistory = donationHistory;
    }
  

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Date getBloodDonateDate() {
        return bloodDonateDate;
    }

    public void setBloodDonateDate(Date bloodDonateDate) {
        this.bloodDonateDate = bloodDonateDate;
    }
    
    
    
}
