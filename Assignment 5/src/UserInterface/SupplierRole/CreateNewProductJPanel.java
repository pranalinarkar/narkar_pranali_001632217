/*
 * CreateProductJPanel.java
 *
 * Created on September 18, 2008, 2:54 PM
 */
package UserInterface.SupplierRole;

import Business.Product;
import Business.Supplier;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 *
 * @author pranalinarkar
 */
public class CreateNewProductJPanel extends javax.swing.JPanel {

    Supplier supplier;
    JPanel userProcessContainer;
    
    /** Creates new form CreateProductJPanel */
    public CreateNewProductJPanel(JPanel userProcessContainer, Supplier supplier){
        initComponents();
        this.supplier = supplier;
        this.userProcessContainer = userProcessContainer;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        idField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtFloorPrice = new javax.swing.JTextField();
        createButton = new javax.swing.JButton();
        backButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        nameField1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtCeilPrice = new javax.swing.JTextField();
        txtTargetPrice = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Create New Product");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Product ID:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(101, 130, 100, 30));

        idField.setEditable(false);
        idField.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(idField, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, 160, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Floor Price:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 210, 110, 30));

        txtFloorPrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtFloorPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 210, 160, 30));

        createButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        createButton.setText("Add Product");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });
        add(createButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 310, -1, -1));

        backButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        backButton1.setText("<< Back");
        backButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButton1ActionPerformed(evt);
            }
        });
        add(backButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 370, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Product Name:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, -1, 30));

        nameField1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(nameField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 160, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Ceil Price:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 250, 110, 30));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Target Price:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 110, 40));

        txtCeilPrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtCeilPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 250, 160, 30));

        txtTargetPrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        add(txtTargetPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 170, 160, 30));
    }// </editor-fold>//GEN-END:initComponents
    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
    // TODO add your handling code here:
    try{   
           for(Product p1: supplier.getProductCatalog().getProductcatalog()){
               if (p1.getProdName().equalsIgnoreCase(nameField1.getText())){
                   JOptionPane.showMessageDialog(null,"Product already exists.","Product already exists.",JOptionPane.WARNING_MESSAGE);
                   
                   return;
                   
               }
           }
        
          
            String stringTargetPrice = txtTargetPrice.getText().trim();
            String stringfloorPrice = txtFloorPrice.getText().trim();
            String stringCeilPrice = txtCeilPrice.getText().trim();
            
            
            
            
            
            
            
            
            
            
            
           if(stringTargetPrice.isEmpty()==false&&stringfloorPrice.isEmpty()==false&&stringCeilPrice.isEmpty()==false){
         
             
               
           
            if(Integer.parseInt(stringTargetPrice)<Integer.parseInt(stringCeilPrice) &&
                    Integer.parseInt(stringTargetPrice)>Integer.parseInt(stringfloorPrice)&&Integer.parseInt(stringfloorPrice)< Integer.parseInt(stringCeilPrice)){
            
            
               
             if(Integer.parseInt(stringTargetPrice)>=0 && Integer.parseInt(stringCeilPrice)>=0 && Integer.parseInt(stringfloorPrice)>=0) {
                 Product product = supplier.getProductCatalog().addProduct();
          
            product.setProdName(nameField1.getText().trim());
                 
                 
                 int targetprice = Integer.parseInt(stringTargetPrice);
             
             
                int floorprice = Integer.parseInt(stringfloorPrice);
                int ceilPrice = Integer.parseInt(stringCeilPrice); 
                product.setPrice(targetprice);
                product.setFloorPrice(floorprice);
                product.setCeilPrice(ceilPrice);
            JOptionPane.showMessageDialog(null, "Created Successfully", "Created Successfully", JOptionPane.INFORMATION_MESSAGE);
            }
             else{
             JOptionPane.showMessageDialog(null, "data should not be negative", "Exception caught", JOptionPane.INFORMATION_MESSAGE);
             
             
             }
            }
            
            else {
            JOptionPane.showMessageDialog(null, "floorprice<target<ceilprice", "Exception caught", JOptionPane.INFORMATION_MESSAGE);
            
            }
           }
          
           else{
            
            JOptionPane.showMessageDialog(null, "feilds are empty", "Created Successfully", JOptionPane.INFORMATION_MESSAGE);
            
           }
    }
    catch(Exception e){
    JOptionPane.showMessageDialog(null, "Invalid data", "Exception caught", JOptionPane.INFORMATION_MESSAGE);
    
    }
    
    
    
     
}//GEN-LAST:event_createButtonActionPerformed

    private void backButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageProductCatalogJPanel manageProductCatalogJPanel = (ManageProductCatalogJPanel) component;
        manageProductCatalogJPanel.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButton1ActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton1;
    private javax.swing.JButton createButton;
    private javax.swing.JTextField idField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField nameField1;
    private javax.swing.JTextField txtCeilPrice;
    private javax.swing.JTextField txtFloorPrice;
    private javax.swing.JTextField txtTargetPrice;
    // End of variables declaration//GEN-END:variables
}
