/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author pranalinarkar
 */
public class SalesPersonDirectory {

    private ArrayList<SalesPerson> salespersonList;

    public SalesPersonDirectory() {
        salespersonList = new ArrayList<SalesPerson>();
    }

    public ArrayList<SalesPerson> getSalespersonList() {
        return salespersonList;
    }

    public void setSalespersonList(ArrayList<SalesPerson> salespersonList) {
        this.salespersonList = salespersonList;
    }

    public SalesPerson addSalesPerson() {
        SalesPerson salesPerson = new SalesPerson();
        salespersonList.add(salesPerson);
        return salesPerson;
    }

}
