/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author pranalinarkar
 */
public class Customer implements Comparable<Customer> {

    private String custName;
    private int customerSalesVolume;
    private int itemCount;

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public int getCustomerSalesVolume() {
        return customerSalesVolume;
    }

    public void setCustomerSalesVolume(int customerSalesVolume) {
        this.customerSalesVolume = customerSalesVolume;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    @Override
    public String toString() {
        return custName;
    }

    @Override
    public int compareTo(Customer o) {
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (this.customerSalesVolume > o.getCustomerSalesVolume()) {
            return -1;
        } else if (this.customerSalesVolume < o.getCustomerSalesVolume()) {
            return 1;
        } else {

            return 0;
        }
    }

}
