/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author pranalinarkar
 */
public class MasterProduct implements Comparable<MasterProduct> {

    private String productName;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return productName;
    }

    @Override
    public int compareTo(MasterProduct t) {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (this.count > t.getCount()) {
            return -1;
        } else if (this.count < t.getCount()) {
            return 1;
        } else {

            return 0;
        }
    }

}
