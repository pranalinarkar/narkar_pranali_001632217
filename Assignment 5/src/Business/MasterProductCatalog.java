/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author pranalinarkar
 */
public class MasterProductCatalog {

    private ArrayList<MasterProduct> masterProductList;
    private MasterProduct masterProduct;

    public MasterProduct getMasterProduct() {
        return masterProduct;
    }

    public void setMasterProduct(MasterProduct masterProduct) {
        this.masterProduct = masterProduct;
    }

    public MasterProductCatalog() {
        masterProductList = new ArrayList<MasterProduct>();
        masterProduct = new MasterProduct();
    }

    public ArrayList<MasterProduct> getMasterProductList() {
        return masterProductList;
    }

    public void setMasterProductList(ArrayList<MasterProduct> masterProductList) {
        this.masterProductList = masterProductList;
    }

    public MasterProduct addProduct(String pname, int pcount) {
        MasterProduct masterProduct = new MasterProduct();
        masterProduct.setProductName(pname);
        masterProduct.setCount(pcount);
        masterProductList.add(masterProduct);
        return masterProduct;
    }

}
