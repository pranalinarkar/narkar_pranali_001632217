package Business;

/**
 *
 */
public class Business {

    private SupplierDirectory supplierDirectory;
    private MasterOrderCatalog masterOrderCatalog;
    private SalesPersonDirectory salesPersonDirectory;
    private CustomerDirectory customerDirectory;
    private ProductCatalog productCatalog;
    private MasterProductCatalog masterProductCatalog;

    public Business() {
        supplierDirectory = new SupplierDirectory();
        masterOrderCatalog = new MasterOrderCatalog();
        salesPersonDirectory = new SalesPersonDirectory();
        customerDirectory = new CustomerDirectory();
        productCatalog = new ProductCatalog();
        masterOrderCatalog = new MasterOrderCatalog();
        masterProductCatalog = new MasterProductCatalog();
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }

    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    public MasterProductCatalog getMasterProductCatalog() {
        return masterProductCatalog;
    }

    public void setMasterProductCatalog(MasterProductCatalog masterProductCatalog) {
        this.masterProductCatalog = masterProductCatalog;
    }

}
