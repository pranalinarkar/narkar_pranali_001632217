package Business;

import java.util.ArrayList;

public class Order {

    private static int count = 0;
    private ArrayList<OrderItem> orderItemList;
    private int orderNumber;
    private Customer customer;


    public Order() {
        count++;
        orderNumber = count;
        orderItemList = new ArrayList<>();
        customer = new Customer();

    }

    public Customer getCustomer() {
        return customer;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void removeOrderItem(OrderItem o) {
        orderItemList.remove(o);
    }


    public OrderItem addOrderItems(Product p, double price, Customer cust, SalesPerson salesPerson) {
        OrderItem o = new OrderItem();
        o.setCustomer(cust);
        o.setSalesPerson(salesPerson);
        o.setProduct(p);
        
        o.setSalesPrice((int) price);
        orderItemList.add(o);
        return o;
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

}
