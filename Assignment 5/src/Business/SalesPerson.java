/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author pranalinarkar
 */
public class SalesPerson implements Comparable<SalesPerson> {

    private String salesName;
    private double commPrice;
    private int cntAboveTarget;
    private int cntBelowTarget;
    private int itemCnt;
    private int salesByVolume;

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public double getCommPrice() {
        return commPrice;
    }

    public void setCommPrice(double commPrice) {
        this.commPrice = commPrice;
    }

    public int getCntAboveTarget() {
        return cntAboveTarget;
    }

    public void setCntAboveTarget(int cntAboveTarget) {
        this.cntAboveTarget = cntAboveTarget;
    }

    public int getCntBelowTarget() {
        return cntBelowTarget;
    }

    public void setCntBelowTarget(int cntBelowTarget) {
        this.cntBelowTarget = cntBelowTarget;
    }

    public int getItemCnt() {
        return itemCnt;
    }

    public void setItemCnt(int itemCnt) {
        this.itemCnt = itemCnt;
    }

    public int getSalesByVolume() {
        return salesByVolume;
    }

    public void setSalesByVolume(int salesByVolume) {
        this.salesByVolume = salesByVolume;
    }


    @Override
    public String toString() {
        return salesName;
    }

    @Override
    public int compareTo(SalesPerson o) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (this.salesByVolume > o.getSalesByVolume()) {
            return -1;
        } else if (this.salesByVolume < o.getSalesByVolume()) {
            return 1;
        } else {

            return 0;
        }
    }

}
